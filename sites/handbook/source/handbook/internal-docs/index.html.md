---
layout: markdown_page
title: Internal Documentation
description: List of internal documentation "sites" for tools developed for internal use.
---

## Welcome to the internal documentation
{: .no_toc}

This section of the handbook is meant to house cross-team documentation for internally developed products or tools, such as LicenseDot or the CustomersDot.

Please create a folder for each application.

* [CustomersDot Admin](/handbook/internal-docs/customers-admin)
