---
layout: markdown_page
title: "Group Direction - Compliance"
canonical_path: "/direction/manage/compliance/"
---

This is a placeholder page for the [Compliance group](https://about.gitlab.com/handbook/product/categories/#compliance-group). Check out the
[Manage section](https://about.gitlab.com/direction/manage) page to see what
we are working on and our individual category pages to see what our group is
working on.

## Categories we focus on
1. [Compliance Management](./compliance-management)
1. [Audit Events](./audit-events)
1. [Audit Reports](./audit-reports)
